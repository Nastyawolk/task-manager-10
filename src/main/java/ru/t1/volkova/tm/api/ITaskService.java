package ru.t1.volkova.tm.api;

import ru.t1.volkova.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
