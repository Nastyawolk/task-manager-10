package ru.t1.volkova.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
