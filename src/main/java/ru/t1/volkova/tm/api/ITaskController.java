package ru.t1.volkova.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
