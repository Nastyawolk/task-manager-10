package ru.t1.volkova.tm.api;

import ru.t1.volkova.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
